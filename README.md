# Simple framework agnostic rbac package written in PHP

[Users](src/Entities/User.php) and [Roles](src/Entities/Role.php) are connected as many-to-many relation
Permissions are simply integers, and every role contains sum of these integers

If you want to integrate this shit, use [AuthorizeService](src/Services/AuthorizeService.php)
to check roles and permissions on each user, and [RoleService](src/Services/RoleService.php) 
to manage roles

Each of thees should have been given an [RoleRepository](src/Repositories/RoleRepository.php) instance,
you may write your own or use existed one [PDORepository](src/Repositories/PDORoleRepository.php) if you use 
one of the sql distribution with my columns and tables names, or extend it and rewrite protected
constants to specify your own names

If somethings is wrong, a [RBACException](src/Exceptions/RBACException.php) will be thrown
