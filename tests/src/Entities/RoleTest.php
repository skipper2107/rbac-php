<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 16.08.18
 * Time: 21:18
 */

namespace Tests\Rbac\Entities;

use PHPUnit\Framework\TestCase;
use Skipper\RBAC\Entities\Role;
use Skipper\RBAC\Exceptions\RBACException;

class RoleTest extends TestCase
{
    public function testCreate()
    {
        $role = new Role('admin', 'Admin');

        $this->assertEquals('admin', $role->getName());
        $this->assertEquals('Admin', $role->getDescription());
        $this->assertNull($role->getId());
        $this->assertTrue($role->isActive());
        $this->assertEquals(0, $role->getPermission());
    }

    public function testGetSet()
    {
        $role = new Role('admin', 'Admin');

        $role->setName('user')
            ->setDescription('User')
            ->setIsActive(false)
            ->setPermission(1)
            ->setId(123);

        $this->assertEquals(123, $role->getId());
        $this->assertEquals('user', $role->getName());
        $this->assertEquals('User', $role->getDescription());
        $this->assertEquals(1, $role->getPermission());
        $this->assertFalse($role->isActive());
    }

    /**
     * @throws RBACException
     */
    public function testPermission()
    {
        $role = new Role('admin', 'Admin');
        $role->grantPermission(1);
        $role->grantPermission(2);
        $role->grantPermission(4);

        $this->assertEquals(7, $role->getPermission());

        $this->assertTrue($role->hasPermission(4));
        $this->assertFalse($role->hasPermission(8));

        $role->revokePermission(4);
        $this->assertFalse($role->hasPermission(4));

        $this->expectException(RBACException::class);
        $this->expectExceptionMessage('Invalid Permission');
        $role->grantPermission(3);
    }
}