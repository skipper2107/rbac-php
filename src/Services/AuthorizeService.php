<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 22.09.17
 * Time: 13:29
 */

namespace Skipper\RBAC\Services;

use Skipper\RBAC\Entities\Role;
use Skipper\RBAC\Entities\User;
use Skipper\RBAC\Exceptions\AccessDeniedException;
use Skipper\RBAC\Repositories\RoleRepository;

class AuthorizeService
{
    /**
     * @var RoleRepository
     */
    protected $roles;

    public function __construct(RoleRepository $roles)
    {
        $this->roles = $roles;
    }

    /**
     * @param string $role
     * @param User $user
     * @return AuthorizeService
     * @throws AccessDeniedException
     */
    public function requireRole(string $role, User $user): AuthorizeService
    {
        /** @var Role $roleEntity */
        foreach ($this->roles->fetchUserRoles($user->getId()) as $roleEntity) {
            if ($roleEntity->getName() === $role) {
                return $this;
            }
        }

        throw new AccessDeniedException();
    }

    /**
     * @param string $role
     * @param User $granter
     * @param User $receiver
     * @return AuthorizeService
     * @throws AccessDeniedException
     */
    public function grantRole(string $role, User $granter, User $receiver): AuthorizeService
    {
        $this->checkUserForPermissionToManageRoles($granter);
        $role = $this->roles->getOneByName($role);
        $this->roles->addRole($receiver->getId(), $role->getId());
        $receiver->setRoles(array_merge($receiver->getRoles(), [$role]));

        return $this;
    }

    protected function checkUserForPermissionToManageRoles(User $user)
    {
        //check for permissions
        //require role or permission
//        $this->requirePermission((int)'PermissionValueToBeRequired', $user);
//        $this->requireRole('RoleNameToBeRequired', $user);
    }

    /**
     * @param string $role
     * @param User $revoker
     * @param User $victim
     * @throws AccessDeniedException
     * @return AuthorizeService
     */
    public function revokeRole(string $role, User $revoker, User $victim): AuthorizeService
    {
        $this->checkUserForPermissionToManageRoles($revoker);
        $role = $this->roles->getOneByName($role);
        $this->roles->revokeRole($victim->getId(), $role->getId());
        $victim->setRoles($this->roles->fetchUserRoles($victim->getId()));

        return $this;
    }

    /**
     * @param int $permission
     * @param User $user
     * @return AuthorizeService
     * @throws AccessDeniedException
     */
    public function requirePermission(int $permission, User $user): AuthorizeService
    {
        $totalPermission = 0;
        /** @var Role $role */
        foreach ($this->roles->fetchUserRoles($user->getId()) as $role) {
            $totalPermission |= $role->getPermission();
        }
        if (false === (bool)($totalPermission & $permission)) {
            throw new AccessDeniedException;
        }

        return $this;
    }
}