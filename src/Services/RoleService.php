<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 12/6/17
 * Time: 3:18 PM
 */

namespace Skipper\RBAC\Services;

use Skipper\RBAC\DataTransferObjects\RoleDTO;
use Skipper\RBAC\Entities\Role;
use Skipper\RBAC\Repositories\RoleRepository;
use Skipper\Repository\Exceptions\StorageException;

class RoleService
{
    /**
     * @var RoleRepository
     */
    protected $roles;

    public function __construct(RoleRepository $repository)
    {
        $this->roles = $repository;
    }

    /**
     * @param RoleDTO $dto
     * @return Role
     * @throws StorageException
     */
    public function createRole(RoleDTO $dto): Role
    {
        $role = new Role();
        $role->setPermission($dto->permission)
            ->setIsActive($dto->isActive)
            ->setDescription($dto->description)
            ->setName($dto->name);
        $this->roles->save($role);

        return $role;
    }

    /**
     * @param RoleDTO $dto
     * @return Role
     * @throws StorageException
     */
    public function updateRole(RoleDTO $dto): Role
    {
        $role = $this->roles->getOneByName($dto->name);
        $role->setIsActive($dto->isActive)
            ->setPermission($dto->permission);
        $this->roles->save($role);

        return $role;
    }
}