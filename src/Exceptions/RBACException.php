<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 12/5/17
 * Time: 4:30 PM
 */

namespace Skipper\RBAC\Exceptions;

use Skipper\Exceptions\DomainException;
use Skipper\Exceptions\Error;

/**
 * Class RBACException
 * @package Skipper\RBAC\Exceptions
 */
class RBACException extends DomainException
{
    protected function addInstantError(): ?Error
    {
        return new Error('Rbac exception', 'authorizationError', 'auth');
    }
}