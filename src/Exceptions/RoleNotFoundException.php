<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 12/5/17
 * Time: 5:33 PM
 */

namespace Skipper\RBAC\Exceptions;

use Skipper\Exceptions\Error;

class RoleNotFoundException extends RBACException
{
    protected function addInstantError(): ?Error
    {
        return new Error('Role not found', 'notFound', 'role');
    }
}