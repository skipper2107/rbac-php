<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 12/5/17
 * Time: 5:40 PM
 */

namespace Skipper\RBAC\Exceptions;

use Skipper\Exceptions\Error;

class StorageException extends RBACException
{
    protected function addInstantError(): ?Error
    {
        return new Error('Storage exception', 'internalError', 'storage');
    }
}