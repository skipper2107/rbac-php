<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 22.09.17
 * Time: 17:18
 */

namespace Skipper\RBAC\Exceptions;

use Skipper\Exceptions\DomainException;
use Skipper\Exceptions\Error;

/**
 * Class AccessDeniedException
 * @package Skipper\RBAC\Exceptions
 */
class AccessDeniedException extends DomainException
{
    protected function addInstantError(): ?Error
    {
        return new Error('Access Denied', 'accessDenied', 'auth');
    }
}