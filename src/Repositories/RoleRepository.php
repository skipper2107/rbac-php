<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 12/5/17
 * Time: 4:35 PM
 */

namespace Skipper\RBAC\Repositories;

use Skipper\RBAC\Entities\Role;
use Skipper\Repository\Contracts\Repository;

interface RoleRepository extends Repository
{
    /**
     * @param string $name
     * @return Role
     */
    public function getOneByName(string $name): Role;

    /**
     * @param int $userId
     * @return Role[]
     */
    public function fetchUserRoles(int $userId): array;

    /**
     * @param int $userId
     * @param int $roleId
     * @return bool
     */
    public function addRole(int $userId, int $roleId): bool;

    /**
     * @param int $userId
     * @param int $roleId
     * @return bool
     */
    public function revokeRole(int $userId, int $roleId): bool;

    /**
     * @param int $roleId
     * @param int $permission
     * @return bool
     */
    public function grantPermission(int $roleId, int $permission): bool;

    /**
     * @param int $roleId
     * @param int $permission
     * @return bool
     */
    public function revokePermission(int $roleId, int $permission): bool;
}