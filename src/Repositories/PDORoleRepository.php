<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 12/5/17
 * Time: 4:57 PM
 */

namespace Skipper\RBAC\Repositories;

use Skipper\RBAC\Entities\Role;
use Skipper\RBAC\Exceptions\RBACException;
use Skipper\RBAC\Exceptions\RoleNotFoundException;
use Skipper\RBAC\Exceptions\StorageException;
use Skipper\Repository\Contracts\Entity;
use Skipper\Repository\CriteriaAwareRepository;
use Skipper\Repository\DataTransferObjects\Filter;

/**
 * Class SqlRoleRepositoryRepository
 * @package Skipper\RBAC\Repositories
 */
class PDORoleRepository extends CriteriaAwareRepository implements RoleRepository
{
    public const NAME_COLUMN = 'name';
    public const DESCRIPTION_COLUMN = 'description';
    public const IS_ACTIVE_COLUMN = 'is_active';
    public const PERMISSIONS_COLUMN = 'permissions';
    public const ID_COLUMN = 'id';
    public const ROLE_PIVOT_COLUMN = 'role_id';
    public const USER_PIVOT_COLUMN = 'user_id';

    /** @var \PDO $pdo */
    protected $pdo;
    /** @var string $table */
    protected $table;
    /** @var string $pivot */
    protected $pivot;

    public function __construct(\PDO $pdo, string $table = 'roles', string $pivot = 'users_roles')
    {
        $pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        $pdo->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_ASSOC);
        $this->pdo = $pdo;
        $this->table = $table;
        $this->pivot = $pivot;
    }

    /**
     * @param int $id
     * @return Role|Entity
     * @throws RoleNotFoundException
     * @throws RBACException
     */
    public function find(int $id): Entity
    {
        return $this->findOneBy([self::FILTER => [self::ID_COLUMN => [self::OPERATOR => '=', self::VALUE => $id]]]);
    }

    /**
     * @param array $criteria
     * @return Role|Entity
     * @throws RoleNotFoundException
     * @throws RBACException
     */
    public function findOneBy(array $criteria): Entity
    {
        /** @var Filter $filter */
        $criterias = $this->getFiltersFromCriteria($criteria);
        $filter = reset($criterias);
        if (false === ($filter instanceof Filter) || '=' !== $filter->getOperator()) {
            throw new RBACException('Empty criteria');
        }
        $sql = sprintf('SELECT * FROM %s WHERE %s = :id LIMIT 1;', $this->table, $filter->getColumn());
        $query = $this->pdo->prepare($sql, ['id' => $filter->getValue()]);
        $obj = $query->fetch();
        if (false === $obj) {
            throw new RoleNotFoundException;
        }
        return $this->mapDbArrayToEntity($obj);
    }

    /**
     * @param array $dbRole
     * @return Role
     */
    protected function mapDbArrayToEntity(array $dbRole): Role
    {
        $r = new Role();
        $r->setName($dbRole[static::NAME_COLUMN])
            ->setDescription($dbRole[static::DESCRIPTION_COLUMN])
            ->setIsActive((bool)$dbRole[static::IS_ACTIVE_COLUMN])
            ->setPermission((int)$dbRole[static::PERMISSIONS_COLUMN])
            ->setId($dbRole[static::ID_COLUMN]);

        return $r;
    }

    /**
     * @param Role|Entity $role
     * @return bool
     */
    public function delete(Entity $role): bool
    {
        $sql = sprintf('DELETE FROM %s WHERE %s = :name', $this->table, static::NAME_COLUMN);
        $statement = $this->pdo->prepare($sql, [
            'name' => $role->getName()
        ]);

        return $statement->execute();
    }

    /**
     * @param string $name
     * @return Role
     * @throws RoleNotFoundException
     * @throws RBACException
     */
    public function getOneByName(string $name): Role
    {
        return $this->findOneBy([self::FILTER => [self::NAME_COLUMN => [self::OPERATOR => '=', self::VALUE => $name]]]);
    }

    /**
     * @param int $userId
     * @param int $roleId
     * @return bool
     * @throws StorageException
     */
    public function revokeRole(int $userId, int $roleId): bool
    {
        $sql = sprintf(
            'DELETE FROM %s WHERE %s = :user AND %s = :role',
            $this->pivot,
            static::USER_PIVOT_COLUMN,
            static::ROLE_PIVOT_COLUMN
        );
        $statement = $this->pdo->prepare($sql, [
            'user' => $userId,
            'role' => $roleId,
        ]);
        if (false === $statement->execute()) {
            throw new StorageException;
        }

        return true;
    }

    /**
     * @param int $roleId
     * @param int $permission
     * @return bool
     * @throws StorageException
     */
    public function grantPermission(int $roleId, int $permission): bool
    {
        $sql = sprintf(
            'UPDATE %s SET %s = %s + :permission WHERE %s = :role',
            $this->table,
            static::PERMISSIONS_COLUMN,
            static::PERMISSIONS_COLUMN,
            self::ID_COLUMN
        );
        $statement = $this->pdo->prepare($sql, [
            'permission' => $permission,
            'role' => $roleId,
        ]);
        if (false === $statement->execute()) {
            throw new StorageException;
        }

        return true;
    }

    /**
     * @param int $roleId
     * @param int $permission
     * @return bool
     * @throws StorageException
     */
    public function revokePermission(int $roleId, int $permission): bool
    {
        $sql = sprintf(
            'UPDATE %s SET %s = %s - :permission WHERE %s = :role',
            $this->table,
            static::PERMISSIONS_COLUMN,
            static::PERMISSIONS_COLUMN,
            self::ID_COLUMN
        );
        $statement = $this->pdo->prepare($sql, [
            'permission' => $permission,
            'role' => $roleId,
        ]);
        if (false === $statement->execute()) {
            throw new StorageException();
        }

        return true;
    }

    /**
     * @param Entity|Role $role
     * @return bool
     * @throws StorageException
     */
    public function save(Entity $role): bool
    {
        return null === $role->getId() ? $this->createRole($role) : $this->updateRole($role);
    }

    /**
     * @param Role $role
     * @return bool
     * @throws StorageException
     */
    private function createRole(Role $role): bool
    {
        $sql = sprintf('INSERT INTO %s (%s, %s, %s, %s) VALUES (:name, :description, :permission, :active)',
            $this->table, static::NAME_COLUMN, static::DESCRIPTION_COLUMN, static::PERMISSIONS_COLUMN,
            static::IS_ACTIVE_COLUMN);
        $statement = $this->pdo->prepare($sql, [
            'name' => $role->getName(),
            'description' => $role->getDescription(),
            'permission' => $role->getPermission(),
            'active' => $role->isActive(),
        ]);
        if (false === $statement->execute()) {
            throw new StorageException;
        }
        //todo set id to new role
        return true;
    }

    /**
     * @param Role $role
     * @return bool
     * @throws StorageException
     */
    private function updateRole(Role $role): bool
    {
        $sql = sprintf('UPDATE %s SET %s = :name, %s = :description, %s = :permission, %s = :active WHERE %s = :id)',
            $this->table, static::NAME_COLUMN, static::DESCRIPTION_COLUMN, static::PERMISSIONS_COLUMN,
            static::IS_ACTIVE_COLUMN, static::ID_COLUMN);
        $statement = $this->pdo->prepare($sql, [
            'name' => $role->getName(),
            'description' => $role->getDescription(),
            'permission' => $role->getPermission(),
            'active' => $role->isActive(),
            'id' => $role->getId(),
        ]);
        if (false === $statement->execute()) {
            throw new StorageException();
        }
        return true;
    }

    /**
     * @param int[] $ids
     * @return Entity[]
     */
    public function getAllByIds(array $ids): array
    {
        return $this->findAll([self::FILTER => [self::ID_COLUMN => [self::OPERATOR => 'in', self::VALUE => $ids]]]);
    }

    /**
     * @param array $criteria
     * @return Role[]
     */
    public function findAll(array $criteria): array
    {
        $filters = $this->getFiltersFromCriteria($criteria);
        if (false === empty($filters)) {
            $sql = sprintf('SELECT * FROM %s ', $this->table);
            foreach ($filters as $filter) {
                $sql .= sprintf(
                    'WHERE %s %s %s',
                    $filter->getColumn(),
                    $this->deductOperator($filter->getOperator()),
                    is_array($filter->getValue()) ? sprintf('(%s)',
                        implode(', ', $filter->getValue())) : $filter->getValue()
                );
            }
            $sql .= ';';
        } else {
            $sql = sprintf('SELECT * FROM %s;', $this->table);
        }
        $roles = $this->pdo->query($sql);
        foreach ($roles->fetchAll() as $role) {
            $result[] = $this->mapDbArrayToEntity($role);
        }

        return $result ?? [];
    }

    /**
     * @param string $operator
     * @return string
     */
    private function deductOperator(string $operator): string
    {
        switch ($operator) {
            case '!=' :
                return '<>';
            case '!in':
                return 'not in';
            case '!like':
                return 'not like';
            default:
                return $operator;
        }
    }

    /**
     * @param int $userId
     * @return Role[]
     */
    public function fetchUserRoles(int $userId): array
    {
        $sql = vsprintf('SELECT * FROM %s LEFT JOIN %s on %s.%s = %s.%s WHERE %s.%s = :user', [
            $this->table,
            $this->pivot,
            $this->table,
            static::ID_COLUMN,
            $this->pivot,
            self::ROLE_PIVOT_COLUMN,
            $this->pivot,
            self::USER_PIVOT_COLUMN,
        ]);
        $query = $this->pdo->prepare($sql, [
            'user' => $userId,
        ]);
        foreach ($query->fetchAll() as $role) {
            $roles[] = $this->mapDbArrayToEntity($role);
        }

        return $roles ?? [];
    }

    /**
     * @param int $userId
     * @param int $roleId
     * @return bool
     * @throws StorageException
     */
    public function addRole(int $userId, int $roleId): bool
    {
        $sql = sprintf(
            'INSERT INTO %s (%s, %s) VALUES(:user, :role)',
            $this->pivot,
            static::USER_PIVOT_COLUMN,
            static::ROLE_PIVOT_COLUMN
        );
        $statement = $this->pdo->prepare($sql, [
            'user' => $userId,
            'role' => $roleId,
        ]);
        if (false === $statement->execute()) {
            throw new StorageException();
        }

        return true;
    }

    /**
     * @param array $criteria
     * @return array
     * ['data' => $data, 'total' => $count] = $repo->getAllWithTotalCount([]);
     */
    public function getAllWithTotalCount(array $criteria): array
    {
        return ['data' => [], 'total' => 0];
    }

    /**
     * @param array $criteria
     * @return int
     */
    public function count(array $criteria): int
    {
        return 1;
    }

    /**
     * @param array $criteria
     * @return bool
     */
    public function exists(array $criteria): bool
    {
        return true;
    }
}