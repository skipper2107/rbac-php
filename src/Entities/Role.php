<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 12/5/17
 * Time: 4:10 PM
 */

namespace Skipper\RBAC\Entities;

use Skipper\RBAC\Exceptions\RBACException;
use Skipper\Repository\Contracts\Entity;
use Skipper\Repository\HasId;

class Role implements Entity
{
    use HasId;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $description;

    /**
     * @var int
     */
    protected $permission = 0;

    /**
     * @var bool
     */
    protected $isActive = true;

    public function __construct(string $name, string $description)
    {
        $this->name = $name;
        $this->description = $description;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->isActive;
    }

    /**
     * @param bool $isActive
     * @return Role
     */
    public function setIsActive(bool $isActive): Role
    {
        $this->isActive = $isActive;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Role
     */
    public function setName(string $name): Role
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return Role
     */
    public function setDescription(string $description): Role
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return int
     */
    public function getPermission(): int
    {
        return $this->permission;
    }

    /**
     * @param int $permission
     * @return Role
     */
    public function setPermission(int $permission): Role
    {
        $this->permission = $permission;
        return $this;
    }

    /**
     * @param int $permission
     * @return Role
     * @throws RBACException
     */
    public function grantPermission(int $permission): Role
    {
        $this->assertPermission($permission);
        $this->permission |= $permission;
        return $this;
    }

    /**
     * @param int $permission
     * @throws RBACException
     */
    private function assertPermission(int $permission)
    {
        $s = log($permission, 2.0);
        if ((int)$s != $s) {
            throw new RBACException('Invalid Permission', [
                'permission' => $permission,
            ]);
        }
    }

    /**
     * @param int $permission
     * @return Role
     * @throws RBACException
     */
    public function revokePermission(int $permission): Role
    {
        $this->assertPermission($permission);
        if ($this->hasPermission($permission)) {
            $this->permission -= $permission;
        }
        return $this;
    }

    /**
     * @param int $permission
     * @return bool
     * @throws RBACException
     */
    public function hasPermission(int $permission): bool
    {
        $this->assertPermission($permission);
        return (bool)($this->permission & $permission);
    }
}