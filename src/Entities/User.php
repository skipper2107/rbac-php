<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 12/5/17
 * Time: 4:02 PM
 */

namespace Skipper\RBAC\Entities;

use Skipper\Repository\Contracts\Entity;
use Skipper\Repository\HasId;

class User implements Entity
{
    use HasId;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var bool
     */
    protected $isActive = true;

    /**
     * @var Role[]
     */
    protected $roles = [];

    public function __construct(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return Role[]
     */
    public function getRoles(): array
    {
        return $this->roles;
    }

    /**
     * @param Role[] $roles
     * @return User
     */
    public function setRoles(array $roles): User
    {
        $this->roles = $roles;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return User
     */
    public function setName(string $name): User
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->isActive;
    }

    /**
     * @param bool $isActive
     * @return User
     */
    public function setIsActive(bool $isActive): User
    {
        $this->isActive = $isActive;
        return $this;
    }
}