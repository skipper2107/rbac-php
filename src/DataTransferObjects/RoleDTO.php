<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 12/6/17
 * Time: 12:10 PM
 */

namespace Skipper\RBAC\DataTransferObjects;


class RoleDTO
{
    /** @var string $name */
    public $name;
    /** @var string $description */
    public $description;
    /** @var int $permission */
    public $permission = 0;
    /** @var bool $isActive */
    public $isActive = false;
}